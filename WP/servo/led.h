enum LedDirection {LEFT, RIGHT};

void LedStepRight(void);
void LedStepLeft(void);
void LedInit(void);
void LedStepLeft(void);
void LedStepRight(void);
