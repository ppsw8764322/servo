void DetectorInit(void);
void ServoInit(unsigned int uiServoFrequency);
void ServoCallib(void);
void ServoGoTo(unsigned int uiPosition);
void ServoShiftPosition(unsigned int uiShiftPosition);
void ServoWait(unsigned int uiseconds);
