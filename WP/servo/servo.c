#include <LPC213x.H>
#include "servo.h"
#include "led.h"
#include "timer_interrupts.h"

#define DETECTOR_bm (1 << 10)

enum ServoState {CALLIB, IDLE, IN_PROGRESS, WAIT, OFFSET};

struct Servo
{
	enum ServoState eState;
	unsigned int uiCurrentPosition; 
	unsigned int uiDesiredPosition;
	unsigned int uiFrequency;
};

struct Servo sServo;

void  DetectorInit(){
	IO0DIR = IO0DIR & (~DETECTOR_bm);
}

enum DetectorState {ACTIVE, INACTIVE};

enum DetectorState eReadDetector(){
	if((IO0PIN & DETECTOR_bm) == 0){
		return ACTIVE;
	}
	else{
		return INACTIVE;
	}
}

void Automat(void){
	switch(sServo.eState){
		case CALLIB:
			if(eReadDetector() == ACTIVE){
				sServo.uiCurrentPosition = 0;
				sServo.uiDesiredPosition = 0;
				sServo.eState = OFFSET;
			}else{
				LedStepLeft();
				sServo.eState = CALLIB;
			}
			break;
   case OFFSET:
			if (sServo.uiCurrentPosition == 12) {
					sServo.uiCurrentPosition = 0;
					sServo.eState = IDLE;
			}else{
				sServo.uiCurrentPosition++;
				LedStepRight();
			}
			break;
		case IDLE:
			if(sServo.uiCurrentPosition != sServo.uiDesiredPosition){
				sServo.eState = IN_PROGRESS;
			}else{
				sServo.eState = IDLE;
			}
			break;
		case IN_PROGRESS:
			if(sServo.uiCurrentPosition < sServo.uiDesiredPosition){
				sServo.uiCurrentPosition++;
				LedStepRight();
				sServo.eState = IN_PROGRESS;
			}else if(sServo.uiCurrentPosition > sServo.uiDesiredPosition){
				sServo.uiCurrentPosition--;
				LedStepLeft();
				sServo.eState = IN_PROGRESS;
			}else{
				sServo.eState = IDLE;
			}
			break;
   case WAIT:
			if (sServo.uiFrequency > 0) {
					sServo.uiFrequency--;
			}else{
					sServo.eState = IDLE;
			}
			break;
		}
}

void ServoInit(unsigned int uiServoFrequency){
	sServo.eState = CALLIB;
	sServo.uiFrequency = uiServoFrequency;
	LedInit();
	Timer0Interrupts_Init((1000000/uiServoFrequency),&Automat);
	while(sServo.eState == CALLIB){}
}

void ServoCallib(void){
	sServo.eState = CALLIB;
}

void ServoGoTo(unsigned int uiPosition){
	sServo.eState = IN_PROGRESS;
	sServo.uiDesiredPosition = uiPosition;
	while(sServo.eState == IN_PROGRESS){}
}

//shift  12 przesunie o 12 
void ServoShiftPosition(unsigned int uiStepSize){
	sServo.uiDesiredPosition = sServo.uiCurrentPosition + uiStepSize;
}

void ServoWait(unsigned int uiSeconds) {
    sServo.uiFrequency = uiSeconds * sServo.uiFrequency; 
    sServo.eState = WAIT;
    while (sServo.eState == WAIT) {}
}
